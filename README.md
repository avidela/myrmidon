# Myrmidon - Making the compiler do real work

This repository is an experiment with partial evaluation of functional programming languages.

## Current status:

Trying to use [Normalisation By Evaluation](https://en.wikipedia.org/wiki/Normalisation_by_evaluation)
in order to partially evaluate programs.
The goal is to control which parts of the programs are going to be reduced by
using different constructors for parts of the programs that will be reduced.

Giving the control to the programmer about what should be partially evaluated
should come with those two benefits:
 - Ensure not too much of the program is partially evaluated to avoid bytecode blowup
 - Produce type errors when the compiler encounter a part of the program that cannot
   be reduced but has been marked by the programmer as being reducible.

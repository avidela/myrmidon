module Untyped

infixr 0 =>>


data Exp = K | S | App Exp Exp | Zero | Succ | Rec

B : Exp
B = App (App S (App K S)) K

C : Exp
C = (S `App` ((S `App` (K `App` B)) `App` S)) `App` (K `App` K)

data D = Lam (D -> D)

app : D -> D -> D
app (Lam f) y = f y

eval : Exp -> D
eval K = Lam (\x => Lam (\y => x))
eval S = Lam (\x => Lam (\y => Lam (\z => app (app x z) (app y z))))
eval (App x y) = app (eval x) (eval y)
eval Zero = Lam (\x => eval K)
eval Succ = Lam (\x => eval K)
eval Rec = Lam (\x => eval K)

data G = G1 Exp (G -> G)

reify : G -> Exp
reify (G1 x f) = x

app' : G -> G -> G
app' (G1 x f) y = f y

eval' : Exp -> G
eval' K = G1 K (\x => G1 (App K (reify x)) (\y => x))
eval' S = G1 S (\x => G1 (App S (reify x))
               (\y => G1 (App (App S (reify x)) (reify y))
               (\z => app' (app' x z) (app' y z))))
eval' (App x y) = app' (eval' x) (eval' y)
eval' Zero = G1 Zero (\x => eval' K)
eval' Succ = G1 Succ (\x => eval' K)
eval' Rec = G1 Rec (\x => eval' K)


nbe : Exp -> Exp
nbe = reify . eval'

data Ty = (=>>) Ty  Ty | ℕat

data T = TVar String
       | TApp T T
       | TLam String Ty T
       | TZero
       | TSucc T
       | TRec Ty T T T

namespace SKI
  public export
  add : Exp
  add = App C (App (App S Rec) (App K (App K Succ)))

  public export
  mult : Exp
  mult = App C (App (App S (App K (App Rec Zero))) (App (App S (App K K)) add))

  public export
  pow : Exp
  pow = App C (App (App S (App K (App Rec (App Succ Zero)))) (App (App S (App K K)) mult))

addT : T -> T -> T
addT x y = TRec ℕat y (TLam "v" ℕat (TSucc (TVar "v"))) x

multT : T -> T -> T
multT m n = TRec ℕat (TZero) (TLam "x" ℕat (TLam "y" ℕat (addT m (TVar "y")))) n

powerT : T -> T -> T
powerT m n = TRec ℕat (TSucc TZero) (TLam "x" ℕat (TLam "y" ℕat (multT m (TVar "y")))) n

One, Two, Three : Exp
One = App Succ Zero
Two = App Succ One
Three = App Succ Two



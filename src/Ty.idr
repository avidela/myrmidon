module Ty

import Decidable.Equality

infixr 7 =>> -- Type in Lambda calculus

-- Types in lambda calculus
public export
data Ty : Type where
  ℕat : Ty
  BBool : Ty
  (=>>) : Ty -> Ty -> Ty

public export
DecEq Ty where
  decEq ℕat ℕat = Yes Refl
  decEq BBool BBool = Yes Refl
  decEq (a =>> b) (a' =>> b') with (decEq a a')
    decEq (a =>> b) (a =>> b') | Yes Refl with (decEq b b')
      decEq (a =>> b) (a =>> b) | Yes Refl | Yes Refl = Yes Refl
      decEq (a =>> b) (a =>> b') | Yes Refl | No contra = No ?noEq2
    decEq (a =>> b) (a' =>> b') | No contra = No ?NoEq3
  decEq _ _ = No ?cannotEqRest

public export
Eq Ty where
  ℕat == ℕat = True
  (a =>> b) == (c =>> d) = a == c && b == d
  _ == _ = False

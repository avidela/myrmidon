module TT.Dependent

import Data.Nat
import Data.Fin
import Data.Vect
import Data.List
import Contexts
import Operators

-- this is O(n)
enumerate : Nat -> Nat -> List Nat
enumerate 0 acc = []
enumerate (S k) acc = acc :: (enumerate k (S acc))

-- this is O(n^2)
enumerateSlow : Nat -> List Nat
enumerateSlow 0 = []
enumerateSlow (S k) = 0 :: map S (enumerateSlow k )

data MyEq : {ty : Type} -> (a, b : ty) -> Type where
  EqItself : MyEq x x

test : Dependent.enumerate 3 0 `MyEq` [0,1,2]
test = EqItself

testSlow : Dependent.enumerateSlow 3 `MyEq` [0,1,2]
testSlow = EqItself

%hide Interfaces.($>)

public export
data Name : Type where
  Global : String -> Name
  Local : Nat -> Name
  Quote : Nat -> Name

Eq Name where
  (Global x) == (Global y) = x == y
  (Local k) == (Local k') = k == k'
  (Quote k) == (Quote k') = k == k'
  _ == _ = False

Show Name where
  show (Global x) = x
  show (Local k) = "local(\{show k})"
  show (Quote k) = "quote(\{show k})"

mutual

  Ty : Type
  Ty = Value

  public export
  data TermInf : Type where
    -- We can infer the type of a term we have to check
    -- if we are given its type
    (.:) : TermChk -> TermChk -> TermInf
    Star : TermInf
    Pi : TermChk -> TermChk -> TermInf
    Bound : Nat -> TermInf
    Free : Name -> TermInf
    ($>) : TermInf -> TermChk -> TermInf
    Nat : TermInf
    NatElim : TermChk -> TermChk -> TermChk -> TermChk -> TermInf

  export
  partial
  Show TermInf where
    show (val .: ty) = "\{show val} : \{show ty}"
    show Star = "*"
    show (Pi arg body) = "PI \{show arg}. \{show body}"
    show (Bound n) = show n
    show (Free n) = "(Free \{show n})"
    show (a $> b) = "\{show a} \{show b}"

  public export
  data TermChk : Type where
    -- We can check a term that we inferred
    TInf : TermInf ->  TermChk
    Lam : TermChk -> TermChk
    Zero : TermChk
    Succ : TermChk -> TermChk

  export
  partial
  Show TermChk where
    show (TInf term) = show term
    show (Lam body) = "λ(\{show body})"

  data Neutral : Type where
    NFree : Name -> Neutral
    (%>) : Neutral -> Value -> Neutral
    NNatElim : Value -> Value -> Value -> Neutral -> Neutral

  data Value : Type where
   VLam : (Value -> Value) -> Value
   VStar : Value
   VPi : Value -> (Value -> Value) -> Value
   VNeutral : Neutral -> Value
   VNat : Value
   VZero : Value
   VSucc : Value -> Value


  partial
  Show Value where
    show (VLam body) = "vlam"
    show VStar = "*"
    show (VPi arg body) = "Pi " ++ show arg ++ ". something"
    show (VNeutral ntr) = "neutral"

  Eq TermChk where
    (TInf x) == (TInf y) = assert_total $ x == y
    (Lam x) == (Lam y)   = assert_total $ x == y
    _ == _ = False

  partial
  Eq TermInf where
    (x .: y)  == (x' .: y') = x == x' && y == y'
    Star      == Star       = True
    (Pi x y)  == (Pi x' y') = x == x' && y == y'
    (Bound k) == (Bound k') = k == k'
    (Free x)  == (Free x')  = x == x'
    (x $> y)  == (x' $> y') = x == x' && y == y'
    _ == _ = False

Context : Type
Context = List (Name, Ty)

vfree : Name -> Value
vfree n = VNeutral (NFree n)

partial
partialIndex : List a -> Nat -> a
partialIndex (x :: xs) 0 = x
partialIndex (x :: xs) (S k) = partialIndex xs k

mutual
  evalInf : TermInf -> List Value -> Value
  evalInf (x .: y) xs = evalChk x xs
  evalInf (Bound k) xs = assert_total (partialIndex xs k)
  evalInf (Free x) xs = vfree x
  evalInf (($>) x y) xs = vapp (evalInf x xs) (evalChk y xs)
  evalInf Star xs = VStar
  evalInf (Pi t t') xs = VPi (evalChk t xs) (\x => evalChk t' (x :: xs))
  evalInf Nat xs = VNat
  evalInf (NatElim m mz ms n) xs =
     rec (evalChk n xs)
     where
        mzVal : Value
        mzVal = evalChk mz xs
        msVal : Value
        msVal = evalChk ms xs
        rec : Value -> Value
        rec VZero = mzVal
        rec (VSucc k) = (msVal `vapp` k) `vapp` rec k
        rec (VNeutral n) = VNeutral (NNatElim (evalChk m xs) mzVal msVal n)
        rec _ = assert_total $ idris_crash "internal error: eval non-natElim"

  vapp : Value -> Value -> Value
  vapp (VLam f)     y = f y
  vapp (VNeutral x) y = VNeutral ((%>) x y)
  vapp VStar        y = ?cannotApplytoStar
  vapp (VPi t t')   y = ?cannotApplyPI
  vapp VZero        y = ?cannotApplyZero
  vapp (VSucc _)    y = ?cannotApplySucc
  vapp VNat         y = ?cannotApplyNat

  evalChk : TermChk -> List (Value) -> Value
  evalChk (TInf x) xs = evalInf x xs
  evalChk (Lam body) xs = VLam (\x => evalChk body ((x :: xs)))
  evalChk Zero xs = VZero
  evalChk (Succ n) xs = VSucc $ evalChk n xs

Result : Type -> Type
Result = Either String

throw : String -> Result a
throw = Left

mutual
  typeInfZero : Context -> TermInf -> Result Ty
  typeInfZero = typeInf Z

  typeInf : Nat -> Context -> TermInf -> Result Ty
  typeInf i gam (e .: ty) = do typeChk i gam ty VStar
                               let t = evalChk ty []
                               typeChk i gam e t
                               pure t
  typeInf i gam Star = pure VStar
  typeInf i gam (Pi t t') =
    do typeChk i gam t VStar
       let ty = evalChk t []
       typeChk (S i) ((Local i, ty) :: gam) (substChk Z (Free (Local i)) t') VStar
       pure VStar
  typeInf i gam (Free x) =
    case lookup x gam of
         Nothing => throw "unknown identifier"
         (Just ty) => pure ty
  typeInf i gam (fun $> arg) = do
    case !(typeInf i gam fun) of
         (VPi t t') => typeChk i gam arg t *> pure (t' (evalChk arg []))
         _ => throw "illegal application"
  typeInf i gam (Bound x) = ?typeInf_rhs_2
  typeInf i gam Nat = pure VStar
  typeInf i gam (NatElim m mz ms n) =
    do typeChk i gam m (VPi VNat (const VStar)) -- check the function has the right type
       let mVal = evalChk m []
       typeChk i gam mz (mVal `vapp` VZero)
       typeChk i gam ms (VPi VNat (\l => VPi (mVal `vapp` l)
                                             (\_ => mVal `vapp` VSucc l)))
       ignore $ typeChk i gam n VNat
       pure (mVal `vapp` evalChk n [])

  typeChk : Nat -> Context -> TermChk -> Ty -> Result ()
  typeChk i gam (TInf x) v = do v' <- typeInf i gam x
                                let t1 = quote Z v
                                let t2 = quote Z v'
                                unless (t1 == t1)
                                  (throw "type mismatch between \{show t1} and \{show t2}")
  typeChk i gam (Lam x) (VPi t t') =
    typeChk (S i) ((Local i, t) :: gam) (substChk Z (Free (Local i)) x) (t' (vfree (Local i)))
  typeChk i gam Zero VNat = pure ()
  typeChk i gam (Succ k) VNat =
    typeChk i gam k VNat
  typeChk _ _ _ _ = throw "type mismatch"

  substInf : Nat -> TermInf -> TermInf -> TermInf
  substInf i r (x .: ty) = substChk i r x .: ty
  substInf i r (Bound x) = if x == i then r else Bound x
  substInf i r (Free x) = Free x
  substInf i r (($>) x y) = ($>) (substInf i r x) (substChk i r y)
  substInf i r Star = Star
  substInf i r (Pi t t') = Pi (substChk i r t) (substChk (S i) r t')
  substInf i r Nat = Nat
  substInf i r (NatElim m mz ms k) =
    NatElim (substChk i r m)
            (substChk i r mz)
            (substChk i r ms)
            (substChk i r k)

  substChk : Nat -> TermInf -> TermChk -> TermChk
  substChk m x (TInf y) = TInf (substInf m x y)
  substChk m x (Lam y) = Lam (substChk (S m) x y)
  substChk m x Zero = Zero
  substChk m x (Succ n) = (Succ (substChk m x n))

  quote : Nat -> Value -> Maybe TermChk
  quote k (VLam f) = [| Lam (quote (S k) (f (vfree (Quote k)))) |]
  quote k (VNeutral x) = TInf <$> (neutralQuote k x)
  quote _ _ = Nothing

  boundFree : Nat -> Name -> TermInf
  boundFree (S i) (Quote k) = Bound (i `minus` k)
  boundFree _ x = Free x

  neutralQuote : Nat -> Neutral -> Maybe TermInf
  neutralQuote k (NFree x) = Just (boundFree k x)
  neutralQuote k ((%>) x y) = [| neutralQuote k x $> quote k y|]
  neutralQuote k (NNatElim m ms mz _) = ?whui

depId : TermInf
depId = Lam (TInf (Bound 0)) .: TInf (Pi (TInf Star)
                                         (TInf (Pi (TInf $ Bound 0)
                                                   (TInf $ Bound 0))))

ctx1 : Context
ctx1 = [(Global "False", VNeutral (NFree (Global "Bool"))), (Global "Bool", VStar)]

replTest : TermInf -> Context -> (Maybe TermChk, Either String Ty)
replTest term ctx = (quote Z (evalInf term []), typeInf Z ctx term)

plus : TermChk
plus = Lam (Lam (TInf $ NatElim ?motive ?base (?ind) ?thu))

{-
id' : TermChk
id' = Lam (TInf (Bound 0))


tFree : String -> Ty
tFree n = TFree (Global n)

free : String -> TermChk
free n = TInf (Free (Global n))

term1 : TermInf
term1 = id' .: (tFree "a" =>> tFree "a") $> free "y"

term2 : TermInf
term2 = const' .: ((tFree "b" =>> tFree "b") =>> (tFree "a" =>> tFree "b" =>> tFree "b")) $> id' $> free "y"

env1, env2 : List (Name, Info)
env1 = [(Global "y", HasType (tFree "a")), (Global "a", HasKind Star)]

env2 = (Global "b", HasKind Star) :: env1

{-
-}

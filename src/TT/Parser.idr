module TT.Parser

import TT.Dependent
import Control.Monad.State

import Data.String.Parser
import Data.List
import Data.List1
import Data.Fin

data Stmt i tinf 
  = Let String i                 -- let x = t
  | Assume (List (String, tinf)) -- assume x :: t, assume x :: *
  | Eval i                       -- ???
  | Out String                   -- ??

Show a => Show b => Show (Stmt a b) where
  show (Let x y) = "let \{show x} = \{show y}"
  show (Assume xs) = "assume \{show xs}"
  show (Eval x) = "eval"
  show (Out x) = "out"

ScopedParser : Type -> Type
ScopedParser = ParseT (Monad.State.State (List String))

interface Liftable (m, n : Type -> Type) where
  lift : m a -> n a

data Token = LET 
           | COLON 
           | Iden String 
           | STAR 
           | ASSUME 
           | EQ 
           | ARR 
           | LAM
           | LParen
           | RParen
           | FORALL


token' : Monad m => String -> Token -> ParseT m Token
token' str tkn = token str >> pure tkn

identifier : Monad m => ParseT m String
identifier = do l <- letter
                rest <- many alphaNum
                _ <- spaces
                pure (pack (l :: rest))

findNatIndex : (a -> Bool) -> List a -> Maybe Nat
findNatIndex f xs = finToNat <$> (findIndex f xs)

many1 : Monad m => ParseT m a -> ParseT m (List1 a)

partial
unsafeTail : List a -> List a
unsafeTail (x :: xs) = xs

mutual 
  parseITerm : ScopedParser TermInf
  parseITerm = parens [| parseCTerm .: (token ":" *> parseCTerm) |]
           <|> token "*" *> pure Star
           <|>  parsePi
           <|> (do var <- identifier
                   case findNatIndex (==var) !(lift get) of
                        Just n => pure (Bound n)
                        Nothing => pure (Free (Global var)))
           <|> parens (do t <- parseITerm 
                          ts <- many parseCTerm
                          pure (foldl ($>) t ts))
  parsePi : ScopedParser TermInf
  parsePi = do (token "forall" <|> token #"\/"#)
               token "("
               x <- identifier
               token ":"
               lift (modify (x ::))
               t <- parseCTerm
               token ")"
               token "."
               t' <- parseCTerm 
               lift (modify (assert_total unsafeTail))
               pure (Pi t t')

  parseCTerm : ScopedParser TermChk
  parseCTerm = parens parseCTerm
           <|> TInf <$> parseITerm
           <|> parseLam 
  parseLam : ScopedParser TermChk
  parseLam = do _ <- token "\\"
                args <- some identifier
                _ <- token "->"
                lift (modify (args ++))
                t <- parseCTerm
                lift (modify (drop (length args)))
                pure (Lam t)

parseLet : ScopedParser (Stmt TermInf TermChk)
parseLet = token "let" *> [| Let (identifier <* token "=" ) 
                                 parseITerm |] 

parseAssume : ScopedParser (Stmt TermInf TermChk)
parseAssume = token "assume" *> (Assume <$> (many parseType) <?> "could not parse assume")
  where
    parseType : ScopedParser (String, TermChk)
    parseType = [| MkPair (identifier <* token ":") parseCTerm |]
 
export
parseStmt : ScopedParser (Stmt TermInf TermChk)
parseStmt = parseLet <|> parseAssume

parseProgram : ScopedParser a -> String -> (Either String a)
parseProgram parser = map fst . runIdentity . evalStateT [] . parseT (parser <* eos)

main : IO ()
main = do putStrLn "Starting to parse"
          let parsed = Parser.parseProgram parseLet #"let id = (\a -> a : (\/ (x : *). *))"# 
                                              --((\a x -> x) : (\/ (a : *). \/ (v : a). a))"# 
          let parsed2 = Parser.parseProgram parseLet #"let id = (\a -> \x -> x : (\/ (a : *). \/ (v : a). a))"# 

          let parsePI = Parser.parseProgram parsePi #"\/ (x : *). *"#
          let parseProg = Parser.parseProgram parseStmt """
            assume (Bool : Type) (False : Bool)
            id Bool
            """
          let parseID = Parser.parseProgram parseStmt """
            assume (a : Type) (y : a)
            ((\x -> x) : a -> a) y
            """
          let parseB = Parser.parseProgram parseStmt """
            assume (a : Type) (y : a)
            ((\x -> x) : a -> a) y
            assume (b : Type)
            ((\x y -> x) : (b -> b) -> a -> b -> b) (\x -> x) y
            """
          putStrLn "parsed : \{show parsed}"
          putStrLn "parsed2 : \{show parsed2}"
          putStrLn "parsed PI : \{show parsePI}"
          putStrLn "parsed ID : \{show parseID}"
          putStrLn "parsed B : \{show parseB}"




{-
-}

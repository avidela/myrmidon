module Typed

import Ty
import public Data.Fin
import Data.Vect
import Data.List
import Data.List.Elem
import Decidable.Equality
-- import Equality
import Control.Order
import Control.Relation
import Contexts
import Data.Nat

%default total

data Stage = Compile | Runtime

namespace SKI 
  public export
  K : a -> b -> a
  K v _ = v
  
  public export
  S : (a -> b -> c) -> (a -> b) -> a -> c
  S f g x = f x (g x)
  
  I : a -> a
  I = (S (K {b=a->a}) K)
  
  B : (b -> c) -> (a -> b) -> a -> c
  B = S (K S) K
  
  C : (a -> b -> c) -> b -> a -> c
  C = (S $ ((S $ (K $ B)) $ S)) $ (K $ K)
  
  public export
  rec : a -> (Nat -> a -> a) -> Nat -> a
  rec x f 0 = x
  rec x f (S k) = f k (rec x f k)
  
  add : Nat -> Nat -> Nat
  -- add n m = rec m (K S) n
  add = C $ ((S $ rec) $ (K $ (K $ S)))
  
  mult : Nat -> Nat -> Nat
  mult = C (S (K (rec 0)) (S (K K) add))
  
  pow : Nat -> Nat -> Nat
  pow = S (K (rec 1)) (S (K K) (SKI.mult))
  
  testAdd : SKI.add 3 4 = 7
  testAdd = Refl
  
  testMult : SKI.mult 3 4 = 12
  testMult = Refl
  
  testPow : SKI.pow 3 2 = 9
  testPow = Refl

data SKI : Ctx n Ty -> Ty -> Type where

  Var  : {g : _} -> Index g ty -> SKI g ty
  App  : {g : _} -> SKI g (a =>> b) -> SKI g a -> SKI g b
  Lam  : {g : _} -> SKI (g :< a) b -> SKI g (a =>> b)
  Zero : {g : _} -> SKI g ℕat
  Succ : {g : _} -> SKI g (ℕat =>> ℕat)
  Rec  : {g : _} -> SKI g a -> SKI g (ℕat =>> a =>> a) -> SKI g (ℕat =>> a)
  -- Rec : SKI g ℕat -> SKI (g :< ℕat) (a =>> a) -> SKI g (ℕat =>> a)
  -- Rec : {0 a : Ty} -> SKI (a =>> (ℕat =>> a =>> a) =>> ℕat =>> a)



atLeastTwo : LT 1 (S (S n))
atLeastTwo = LTESucc (LTESucc LTEZero)

namespace Programs
  const : {a, b, c : Ty} -> {n : Nat} -> {g : Ctx n Ty} -> SKI g ((a =>> b) =>> c =>> a =>> b)
  const = Lam (Lam (Var 1))

  public export
  add : {n : Nat} -> {g : Ctx n Ty} -> SKI g (ℕat =>> ℕat =>> ℕat)
  add = Lam (Rec (Var 0) (Lam Succ))
     
  public export
  mult : {n : Nat} -> {g : Ctx n Ty} -> SKI g (ℕat =>> ℕat =>> ℕat)
  mult = Lam (Rec Zero (Lam (App add (Var 1))))

  public export
  pow : {n : Nat} -> {g : Ctx n Ty} -> SKI g (ℕat =>> ℕat =>> ℕat)
  pow = Lam (Rec (App Succ Zero) (Lam (App mult (Var 1))))

identity : {a : Ty} -> {n : Nat} -> {g : Ctx n Ty} -> SKI g (a =>> a)
identity = Lam (Var 0)

InterpTy : Ty -> Type
InterpTy ℕat = Nat
InterpTy (a =>> b) = InterpTy a -> InterpTy b
InterpTy BBool = Bool

interpSKI : {g : Ctx Z Ty} -> SKI g ty -> InterpTy ty
interpSKI (Var idx) = absurd idx
interpSKI (Lam body) {g=[<]} = \arg => let a = ?rest arg in ?erst
interpSKI (App f e) = (interpSKI f) (interpSKI e)
interpSKI Zero = Z
interpSKI Succ = S
interpSKI (Rec base step) = SKI.rec (interpSKI base) (interpSKI step)

-- interpTest : Typed.interpSKI (App (App Programs.add (App Succ Zero)) (App Succ Zero)) = 4
-- interpTest = ?whut

{-

RefiTy : Ty -> Type
RefiTy ℕat = SKI ℕat
RefiTy (a =>> b) = (SKI (a =>> b), RefiTy a -> RefiTy b)
RefiTy BBool = SKI BBool

reify : (a : Ty) -> RefiTy a -> SKI a
reify ℕat x = x
reify (y =>> z) (x, w) = x
reify BBool x = x

toSKI : Nat -> SKI ℕat
toSKI 0 = Zero
toSKI (S k) = App Succ (toSKI k)

fromNat : (SKI ℕat -> a -> a) -> Nat -> a -> a
fromNat f n = f (toSKI n )

rec : a -> (SKI ℕat -> a -> a) -> SKI ℕat -> a
rec x f nat = let n = interpSKI nat  in SKI.rec x (fromNat f) n

app' : (_, a -> b) -> a -> b
app' (x, z) y = z y

interpTm : {a : Ty} -> SKI a -> RefiTy a
interpTm K = (K, \p => (App K (reify _ p), const p))
interpTm S = (S, \case (x, y) => (App S x, \(z1, z2) => (App (App S x) z1, \a' => let (y1, y2) = y a' in y2 (z2 a'))))
interpTm (App x y) = let (_, f) = interpTm x in f (interpTm y)
interpTm Zero = Zero
interpTm Succ = (Succ, App Succ)
interpTm Rec = (Rec, \p => (App Rec (reify _ p), \(q, q') => (App (App Rec (reify _ p)) q , rec p (\n, r => q' n `app'` r))))

testInterp : Typed.interpTm (App (App SKI2.add STwo) STwo) === SFour
testInterp = Refl

--testInterpPow : Typed.interpTm (App (App C SKI2.pow) STwo) === (SFour, fn)


{-
mutual
  public export
  data Neutral : Prog g -> Type where

    Term : (x : Elem g) -> Neutral (^x)
    App : Neutral l -> Normal m -> Neutral (l $> m)
    TIf : Normal if_ -> Neutral then_ -> Neutral else_ -> Neutral (PIf if_ then_ else_)


  public export
  data Normal : Prog g -> Type where

    TTrue : Normal PTrue
    TFalse : Normal PFalse
    Term' : Neutral m -> Normal m
    Lam : (n : Prog (S g)) -> Normal n -> Normal (\\ n)

public export
data (~>) : Prog g -> Prog g -> Type where

  Xi : l ~> l' -> l $> m ~> l' $> m

  Xi2 : m ~> m' -> l $> m ~> l $> m'

  Beta : (\\ n) $> m ~> substOne n m

  Zeta : n ~> n' -> \\ n ~> \\ n'

  IfCondApp : c ~> c' -> PIf c t e ~> PIf c' t e

namespace TransitiveClosure
  public export
  data (->>) : Prog g -> Prog g -> Type where
    Step : {m : _} -> m ~> n -> m ->> n
    ReduceRefl : (t : Prog g) -> t ->> t
    ReduceTrans : {l, m, n : _} -> l ->> m -> m ->> n -> l ->> n

  public export
  Transitive (Prog g) (->>) where
    transitive = ReduceTrans
  Reflexive (Prog g) (->>) where
    reflexive = ReduceRefl _ 

data Progress : Prog g -> Type where
  Step : m ~> n -> Progress m
  Done : Normal m -> Progress m


progress : (t : Prog g) -> Progress t
progress PTrue = Done TTrue
progress PFalse = Done TFalse
progress (PIf if_ then_ else_) = ?profress_if
progress (CIf if_ then_ else_) = ?profress_compile_if
progress (^ x) = Done (Term' $ Term x)
progress (\\ x) with (progress x)
  progress (\\ x) | (Step y) = Step (Zeta y)
  progress (\\ x) | (Done y) = Done (Lam x y)
progress (PTrue $> y) = ?fail1
progress (PFalse $> y) = ?fail2
progress (PIf if_ then_ else_ $> y) = ?fail3
progress (CIf if_ then_ else_ $> y) = ?fail4
progress ((\\ x) $> y) = Step Beta

progress ((^ x) $> y) with (progress y)
  progress ((^ x) $> y) | (Step z) = Step (Xi2 z)
  progress ((^ x) $> y) | (Done z) = Done (Term' (App (Term x) z))

progress ((x $> z) $> y) with (progress (x $> z))
  progress ((x $> z) $> y) | (Step w) = Step (Xi w)
  progress ((x $> z) $> y) | (Done w) with (progress y)
    progress ((x $> z) $> y) | (Done w) | (Step v) = Step (Xi2 v)
    progress ((x $> z) $> y) | (Done (Term' w)) | (Done v) =
      Done (Term' (App w v))

{-
namespace Evaluator
  public export
  Gas : Type
  Gas = Nat

  public export
  data Finished : Prog g -> Type where
    Done : Normal n -> Finished n
    Out : Finished n

data Steps : Prog g -> Type where
 MkSteps : l ->> n -> Finished n -> Steps l

eval : Gas -> (l : Prog g) -> Steps l
eval 0 l = MkSteps (ReduceRefl l) Out
eval (S k) l with (progress l)
  eval (S k) l | (Done x) = MkSteps (ReduceRefl l) (Done x)
  eval (S k) m | (Step x) with (eval k m)
    eval (S k) m | (Step x) | (MkSteps y z) =
      MkSteps y z

zero' : {g : _} -> Prog g
zero' = \\ \\ ^0

suc' : {g : _} -> Prog g -> Prog g
suc' m = (\\ \\ \\ (^1 $> ^2)) $> m

case' : {g : _} -> Prog g -> Prog g -> Prog (S g) -> Prog g
case' l m n = l $> (\\ n) $> m

mu : {g : _} -> Prog (S g) -> Prog g
mu x = (\\ ((\\ (^1 $> (^0 $> ^0))) $> (\\ (^1 $> (^0 $> ^0))))) $> (\\ x)

two' : {g : _} ->  Prog g
two' = suc' (suc' zero')

four' : {g : _} -> Prog g
four' = suc' (suc' (suc' (suc' zero')))

plus' : {g : _} -> Prog g
plus' = mu $ \\ \\ case' (^1) (^0) (suc' $ (^3) $> (^0) $> (^1))

mult' : {g : _} -> Prog g
mult' = mu $ \\ \\ case' (^1) (suc' zero')
                              (plus' $> (^1) $> ((^3) $> (^0) $> (^1)))
ifProg : Prog 0 
ifProg = \\ \\ \\ (^0 $> ^1 $> ^2)

false : Prog 0
false = \\ \\ ^1

true : Prog 0
true = \\ \\ ^0

mainProg : Prog 0 
mainProg = ifProg $> twoChurch $> fourChurch $> true

-- test : Typed.eval 100 Typed.mainProg === Typed.eval 100 Typed.twoChurch
-- test = Refl


    {-
public export
appL_cong : {l, l', m : _}
         -> l ->> l'
         -> l $> m ->> l' $> m
appL_cong (Step x) = Step (Xi x)
appL_cong (ReduceRefl l') = ReduceRefl (l' $> m)
appL_cong (ReduceTrans ((Step x)) y) =
  ReduceTrans (Step (Xi x)) (appL_cong y)
appL_cong (ReduceTrans ((ReduceRefl l)) y) =
  ReduceTrans (ReduceRefl (l $> m)) (appL_cong y)
appL_cong (ReduceTrans ((ReduceTrans x z)) y) =
  ReduceTrans (ReduceTrans (appL_cong x)
                           (ReduceTrans (appL_cong z)
                                        (ReduceRefl _)))
                                        (appL_cong y)

public export
appR_cong : {l, m', m : _}
         -> m ->> m'
         -> l $> m ->> l $> m'
appR_cong (Step x) = Step (Xi2 x)
appR_cong (ReduceRefl l') = ReduceRefl (l $> m)
appR_cong (ReduceTrans ((Step x)) y) =
  ReduceTrans (Step (Xi2 x)) (appR_cong y)
appR_cong (ReduceTrans ((ReduceRefl l)) y) = ?asjiod
  ReduceTrans (ReduceRefl (_ $> _)) (appR_cong y)
appR_cong (ReduceTrans ((ReduceTrans x z)) y) =
  ReduceTrans (ReduceTrans (appR_cong x)
                           (ReduceTrans (appR_cong z)
                                        (ReduceRefl _)))
                                        (appR_cong y)

public export
abs_cong : n ->> n' -> \\ n ->> \\ n'
abs_cong (Step x) = Step (Zeta x)
abs_cong (ReduceRefl n') = ReduceRefl (\\ n')
abs_cong (ReduceTrans x y) =
  ReduceTrans (abs_cong x) (abs_cong y)

{-
-}

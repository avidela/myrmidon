module Operators

prefix 5 \\     -- lambda abstraction
infixl 7 $>, %> -- Different function applications
prefix 9 ^      -- Variable
infixl 9 ^^     -- 

infixr 0 =>> -- type constructors for function types
infixr 0 ==> -- function application in the meta-language like $
infixr 4 ~>  -- Single Reduction step
infixr 2 ->> -- "Reduces to"

infix 6 `∋` -- Checked / inherited
infix 6 `∈` -- inferred / synthesized

infixr 8 `X` -- Product in Lambda calculus
infixl 5 ?=  -- Type equality in lambda calculus

infixl 8 .: -- typed term
infixr 4 >: -- `in`
infixl 5 &. -- extend context
infixl 5 |- -- judgement Inferred
infixl 5 |= -- judgement Check
prefix 10 # -- Debrjn index from integer


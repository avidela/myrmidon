||| An untyped lambda calculus
module Lambda

import Data.Fin
import Data.Vect

data Stage = Compile | Runtime


-- data Ctx : Type where
--   Here : Ctx
--   There : 

data Term : Nat -> Type where
  App : Term n -> Term n -> Term n
  Abs : Term (S n) -> Term n
  Var : Stage -> (n : Nat) -> Term n

parameters {0 n : Nat} (context : Vect n ())
  partialEval : Term n -> (Term n)
  partialEval (App (App x z) y) = ?partialEval_rhs_2
  partialEval (App (Abs x) y) = ?partialEval_rhs_3
  partialEval (App (Var Compile idx) y) = ?partialEval_rhs_4
  partialEval (App (Var Runtime idx) y) = App (Var Runtime n) (partialEval y)
  partialEval (Abs x) = (Abs x)
  partialEval (Var x n) = (Var x n)






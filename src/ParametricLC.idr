module ParametricLC

import Ty
import Contexts

record LambdaTypes ty where
  constructor MkLambdaTypes
  boolType : ty
  natType : ty
  fnType : ty -> ty -> ty

noTypes : LambdaTypes Unit
noTypes = MkLambdaTypes MkUnit MkUnit (const (const MkUnit))

typed : LambdaTypes Ty
typed = MkLambdaTypes ℕat BBool (=>>)

parameters {0 types : LambdaTypes ty}
  public export
  data Prog : Ctx ty -> ty -> Type where
  
    -- The True literal 
    PTrue : {0 Γ : Ctx ty} -> Prog Γ types.boolType
  
    -- The False literal
    PFalse : {0 Γ : Ctx ty} -> Prog Γ types.boolType
  
    -- The runtime If
    PIf : forall Γ. {a : ty} -> (if_ : Prog Γ types.boolType) ->  (then_, else_ : Prog Γ a) -> Prog Γ a
    
    -- The compile-time If
    CIf : forall Γ. {a : ty} -> (if_ : Prog Γ types.boolType) -> (then_, else_ : Prog Γ a) -> Prog Γ a

    PZero : forall Γ. Prog Γ types.natType

    PSucc : forall Γ. Prog Γ types.natType -> Prog Γ types.natType

    PRec : forall Γ. {a : ty} -> Prog Γ a -> Prog Γ (types.fnType types.natType (types.fnType a a)) -> Prog Γ (types.fnType a a)
  
    -- Variables
    (^) : forall Γ. (idx : Index Γ tt) -> Prog Γ tt
  
    -- Abstaction
    (\\)  : forall Γ. {a, b  : ty} -> Prog (Γ :< a) b ->  Prog Γ (types.fnType a b)
  
    -- Application
    ($>) : forall Γ. {a, b : ty} 
         -> Prog Γ (types.fnType a b)
         -> Prog Γ a 
         -> Prog Γ b

UnTyped : Ctx Unit -> Type
UnTyped ctx = Prog {types=noTypes} ctx MkUnit

Typed : Ctx Ty -> Ty -> Type
Typed ctx tpe = Prog {types=typed} ctx tpe

-- incorrectTypecheck : Typed [<] ?
-- incorrectTypecheck = PIf PZero PZero PZero

correctApp : UnTyped [<]
correctApp = PTrue $> PFalse

-- incorrectApp : Typed [<] ?
-- incorrectApp = PTrue $> PFalse

-- impossibleTypecheck : Typed [<] ℕat
-- impossibleTypecheck = PTrue $> PFalse

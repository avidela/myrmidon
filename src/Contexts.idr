module Contexts

import public Data.Fin
import Data.So
import Data.Nat

public export
NatCtx : Type
NatCtx = Nat 

public export
NatIdx : NatCtx -> Type
NatIdx = Fin

public export
data Ctx : Nat -> Type -> Type where
  Lin : Ctx Z e
  (:<) : Ctx n e -> e -> Ctx (S n) e

%name Ctx gam, del, sig

public export
lookup : Eq a => a -> Ctx n (a, b) -> Maybe b
lookup v [<] = Nothing
lookup v (x :< (key, value)) with (key == v)
  lookup v (x :< (key, value)) | False = lookup v x
  lookup v (x :< (key, value)) | True = Just value

public export
map : (a -> b) -> Ctx n a -> Ctx n b
map f [<] = [<]
map f (x :< y) = map f x :< f y

length : Ctx n e -> Nat
length [<] = 0
length (x :< y) = S (Contexts.length x)

prfLength : (ctx : Ctx n e) -> Contexts.length ctx === n
prfLength [<] = Refl
prfLength (x :< y) = cong S (prfLength x)

public export
data Index : {0 n : Nat} -> Ctx n ty -> (0 v : ty) -> Type where
  Here : (v : ty) -> Index (g :< v) v
  There : Index g v -> Index (g :< v') v

public export
(g : Ctx Z ty) => Uninhabited (Index g v) where
  uninhabited (Here x) {g=[<]} impossible
  uninhabited (There x) {g=[<]} impossible

-- Uninhabited (Index [<] v) where
--   uninhabited (Here x) impossible
--   uninhabited (There x) impossible

public export
mapIndex : {g : Ctx n a} -> (f : a -> b) -> Index g v -> Index (Contexts.map f g) (f v)
mapIndex f x {g = [<]} = absurd x
mapIndex f (Here v) {g = (y :< v)} = Here (f v)
mapIndex f (There x) {g = (y :< z)} = There (mapIndex f x)

indexToNat : Index g ty -> Nat
indexToNat (Here elem) = Z
indexToNat (There x) = S (indexToNat x)

public export
index : {0 ty : Type} -> {0 v : ty} -> {g : Ctx len ty} -> Index g v -> ty
index (Here v) = v
index (There x) = index x

indexSmaller : {0 v : ty} -> {0 g : Ctx n ty} -> (idx : Index g v) 
            -> Contexts.indexToNat idx `LT` n
indexSmaller (Here elem) = LTESucc LTEZero
indexSmaller (There x) = LTESucc (indexSmaller x)

public export
indexNat : {0 ty : Type} -> {g : Ctx len ty} 
  -> (n : Nat) -> {auto prf : n `LT` len} -> ty
indexNat n {prf = prf} {g = [<]} = absurd prf
indexNat n {prf = (LTESucc z)} {g = (x :< y)} = 
  case z of 
       LTEZero => y
       (LTESucc w) => indexNat _ {prf=LTESucc w} {g=x} 

public export
natToIndex : (n : Nat) -> {0 ty : Type} 
  -> {g : Ctx len ty} -> {auto check : n `LT` len} 
  -> Index g (Contexts.indexNat {g} n)
natToIndex n {g = [<]} {check} = absurd check
natToIndex n {g = (x :< y)} {check = (LTESucc z)} = 
  case z of 
       LTEZero => Here y
       (LTESucc w) => There (natToIndex _ {g=x} {check=LTESucc w})


public export
fromInteger : {g : Ctx len ty} -> 
              (i : Integer) -> (bound : fromInteger i `LT` len) => 
              Index g (Contexts.indexNat {g} (fromInteger i))
fromInteger i {g} {bound = bound} = natToIndex (fromInteger i)

testFromInteger : 3 === (the (Index [< "a", "b", "c", "d"] "a") (There (There (There (Here "a")))))
testFromInteger = Refl

module TypedConversion

%default total 

infixr 2 =>>

data Ty a = Ex a | (=>>) (Ty a) (Ty a)

data Ctx : a -> Type where
  Empty : Ctx a
  More : a -> Ctx a ->  Ctx a

ToIdris : (a -> Type) -> (Ty a) -> Type
ToIdris f (Ex x) = f x
ToIdris f (x =>> y) = ToIdris f x -> ToIdris f y

data Index : Ctx (Ty a) -> Ty a -> Type where
  Here : (x : Ty a) -> Index (More x g) x
  There : Index g x -> Index (More y g) x

data TLC : (ctx : Ctx (Ty a)) -> (ty : (Ty a)) -> Type where

  App : TLC g (a =>> b) -> TLC g a -> TLC g b

  Lam : {0 p, q : Ty a} -> TLC (More p g) q -> TLC g (p =>> q)

  Var : Index g t -> TLC g t

data Env : (a -> Type) -> Ctx (Ty a) -> Type where
  Nil : Env interp Empty
  (::) : ToIdris interp ty -> Env interp g -> Env interp (More ty g)

%name Env env, env1, env2

lookup : Index g ty -> Env interp g -> ToIdris interp ty
lookup (Here ty) (x :: y) = x
lookup (There x) (y :: z) = lookup x z

eval : TLC g ty -> Env interp g -> ToIdris interp ty
eval (App x y) env = (eval x env) (eval y env)
eval (Lam x) env = \y => eval x (y :: env)
eval (Var x) env = lookup x env

namespace SKI 
  public export
  K : a -> b -> a
  K v _ = v
  
  public export
  S : (a -> b -> c) -> (a -> b) -> a -> c
  S f g x = f x (g x)
  
  public export
  I : a -> a
  I = (S (K {b=a->a}) K)
  
  B : (b -> c) -> (a -> b) -> a -> c
  B = S (K S) K
  
  C : (a -> b -> c) -> b -> a -> c
  C = (S $ ((S $ (K $ B)) $ S)) $ (K $ K)

data Comb : (ctx : Ctx (Ty a)) -> (ty : Ty a) -> (sema : Env interp ctx -> ToIdris interp ty) -> Type where

  CApp : Comb g (a =>> b) f -> Comb g a v -> Comb g b (\env => (f env) (v env))

  CVar : (idx : Index g t) -> Comb g t (\env => lookup idx env)

  K : Comb g (a =>> b =>> a) (const SKI.K)
 
  S : Comb g ((a =>> b =>> c) =>> (a =>> b) =>> a =>> c) (const SKI.S)
  
  I : Comb g (a =>> a) (\_, x => x)

lambda : Comb (More s g) ty f -> Comb g (s =>> ty) (\env, x => f (x :: env))
lambda (CApp x y) = CApp (CApp S (lambda x)) (lambda y)
lambda (CVar (Here ty)) = I
lambda (CVar (There x)) = CApp K (CVar x)
lambda K = CApp K K
lambda S = CApp K S
lambda I = CApp K I

translate : (t : TLC g ty) -> Comb g ty (TypedConversion.eval t)
translate (App x y) = CApp (translate x) (translate y)
translate (Lam x) = lambda (translate x)
translate (Var x) = CVar x

PopOne : TLC (More x g) ty -> TLC g (x =>> ty)
PopOne (App y z) = Lam (App y z)
PopOne (Lam y) = Lam (PopOne y)
PopOne (Var (Here ty)) = Lam (Var (Here ty))
PopOne (Var (There y)) = Lam (Var (There y))

-- ToEmptyCtx : {g : _} -> TLC g ty -> TLC Empty (ToType g ty)
-- ToEmptyCtx x {g = Empty} = x
-- ToEmptyCtx x {g = (More tt y)} = 
--   let popped = PopOne x 
--       rec = ToEmptyCtx popped
--       rev = reverseArgs rec Refl
--    in rev

translateOne : (t : TLC (More x g) ty) -> Comb g (x =>> ty) (TypedConversion.eval (PopOne t))
translateOne t with (PopOne t)
  translateOne t | t' = translate t'

ToTypes : Ctx (Ty a) -> Ty a -> Ty a
ToTypes Empty y = y
ToTypes (More x z) y = x =>> ToTypes z y

translateAll : (t : TLC (More x g) ty) -> Comb g (ToTypes (More x g) ty) (TypedConversion.eval (PopOne t))
-- translateAll t = ?translateAll_rhs

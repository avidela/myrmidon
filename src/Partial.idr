module Partial

import Contexts
import Ty
import Operators

data Stage 
  = RT -- runtime
  | CT -- compile-time
  | Combine Stage Stage

record AnnTy where
  constructor MkAnnTy
  ann : Stage
  typ : Ty

-- Combine : Stage -> Stage -> Stage
-- Combine RT _ = RT
-- Combine _ RT = RT
-- Combine _ _ = CT

public export
data Prog : Ctx AnnTy  -> AnnTy -> Type where

  -- The True literal 
  PTrue : forall Γ, ann. Prog Γ (MkAnnTy ann BBool)

  -- The False literal
  PFalse : forall Γ. Prog Γ (MkAnnTy ann BBool)

  PIf : forall Γ. (if_ : Prog Γ (MkAnnTy a BBool)) ->  (then_, else_ : Prog Γ (MkAnnTy b br)) -> Prog Γ (MkAnnTy (Combine a b) br)

  PZero : forall Γ. Prog Γ (MkAnnTy ann ℕat)

  PSucc : forall Γ. Prog Γ (MkAnnTy ann ℕat) -> Prog Γ (MkAnnTy ann ℕat)

  -- Program-time rec
  PRec : forall Γ. Prog Γ (MkAnnTy ann ℕat) -> Prog Γ (MkAnnTy ann ℕat) -> Prog Γ (MkAnnTy ann ℕat)

  -- Variables
  (^) : forall Γ. (idx : Index Γ ty) -> Prog Γ ty

  -- Abstaction
  (\\)  : forall Γ. Prog (Γ :< MkAnnTy (Combine p q) g) (MkAnnTy p b) ->  Prog Γ (MkAnnTy p (g =>> b)) -- g has usage `q` but we can't show that 

  -- Application
  ($>) : forall Γ
       .  Prog Γ 
       -> Prog Γ
       -> Prog Γ


{-
-- export
-- twoChurch : Typed n ((ℕat =>> ℕat) =>> ℕat =>> ℕat)
-- twoChurch = \\ \\ ^(FS FZ) $> (^(FS FZ) $> ^FZ)
-- 
-- fourChurch : Typed n ((ℕat =>> ℕat) =>> ℕat =>> ℕat)
-- fourChurch = \\ \\ (^1 $> (^1 $> (^1 $> (^1 $> ^0))))

-- λf. λg. λh. λx. f h (g h x)
-- public export
-- plusChurch : Typed []
-- plusChurch = \\ \\ \\ \\ (^(There $ There $ There Here) $> ^(There Here) $> (^(There $ There Here) $> ^(There Here) $> ^Here))

-- twoPlusTwoChurch : Prog Z
-- twoPlusTwoChurch = plusChurch $> twoChurch $> twoChurch

ext : (Fin g -> Fin d) -> Fin (S g) -> Fin (S d)
ext f FZ = FZ
ext f (FS x) = FS (f x)

public export
rename : {0 g, d : _} -> (Fin g -> Fin d) -> Prog g -> Prog d
rename f PTrue = PTrue 
rename f PFalse = PFalse
rename f PZero = PZero
rename f (PSucc x) = PSucc (rename f x)
rename f (PRec v r) = PRec (rename f v) (rename f r)
rename f (CRec v r) = CRec (rename f v) (rename f r)
rename f (PIf if_ then_ else_) = PIf (rename f if_) (rename f then_) (rename f else_)
rename f (CIf if_ then_ else_) = CIf (rename f if_) (rename f then_) (rename f else_)
rename f (^ x) = ^ (f x)
rename f (\\ x) = \\ rename (ext f) x
rename f (x $> y) = rename f x $> rename f y
rename f (x %> y) = rename f x %> rename f y

public export
exts : (Fin g -> Prog d) -> Fin (S g) -> Prog (S d)
exts f FZ = ^FZ
exts f (FS y) = rename FS (f y)

public export
subst : {0 g : _} -> (Fin g -> Prog d) ->  Prog g -> Prog d 
subst f PTrue = PTrue
subst f PFalse = PFalse
subst f (PIf if_ then_ else_) = PIf (subst f if_) (subst f then_) (subst f else_)
subst f (CIf if_ then_ else_) = CIf (subst f if_) (subst f then_) (subst f else_)
subst f (^ x) = f x
subst f (\\ x) = \\ (subst (exts f) x)
subst f (x $> y) = subst f x $> subst f y
subst f (x %> y) = subst f x %> subst f y
subst f PZero = PZero
subst f (PSucc n) = PSucc (subst f n)
subst f (PRec n rec) = PRec (subst f n) (subst f rec)
subst f (CRec n rec) = CRec (subst f n) (subst f rec)

public export
substZero : Prog g -> Fin (S g) -> Prog g
substZero x FZ = x
substZero x (FS y) = ^ y

public export
substOne : Prog (S g) -> Prog g -> Prog g
substOne x y = subst (substZero y)  x

mutual
    covering
    computeValue : Prog n -> Maybe (Prog n)
    computeValue PZero = Just PZero
    computeValue (PSucc n) = PSucc <$> computeValue n
    computeValue PTrue = Just PTrue
    computeValue PFalse = Just PFalse
    computeValue (PIf if_ then_ else_) = case computeValue if_ of
                                              Just PTrue => computeValue then_
                                              Just PFalse => computeValue else_
                                              _ => Nothing
    computeValue (^ x) = Nothing
    computeValue (\\ x) = Just (\\ x)
    computeValue ((\\ x) $> y) = computeValue y >>= (computeValue . substOne x) 
    computeValue _ = Nothing
    
    covering
    partialEval : Prog n -> Prog n
    partialEval PZero = PZero
    partialEval (PSucc n) = PSucc (partialEval n)
    partialEval PTrue = PTrue
    partialEval PFalse = PFalse
    partialEval (PRec n f) = PRec n f
    partialEval (CRec n f) = CRec n f
    partialEval (PIf if_ then_ else_) = PIf if_ then_ else_
    partialEval (CIf if_ then_ else_) = let cond = partialEval if_ in
                                            case computeValue cond of
                                                 Just PTrue => partialEval then_
                                                 Just PFalse => partialEval else_
                                                 _ => PIf cond (partialEval then_) (partialEval else_)
    partialEval (^ x) = (^x) 
    partialEval (\\ x) = \\ partialEval x
    partialEval ((\\ x) $> y) = let arg = partialEval y in
                                    case computeValue arg of 
                                     Just val => partialEval (substOne x val)
                                     Nothing => partialEval (\\ x) $> arg
  
    partialEval ((CRec n f) $> PZero) = ((CRec n f) $> PZero)
    partialEval ((PRec n f) %> PZero) = n

    -- unrolling the loop
    partialEval ((PRec n f) %> (PSucc n')) = (f $> n') $> ((PRec n f) $> n')
    partialEval ((CRec n f) %> (PSucc n')) = partialEval (f %> n') $> partialEval ((CRec n f) $> n')
    partialEval ((CRec n f) $> (PSucc n')) = (partialEval (CRec n f) $> (PSucc n'))
    partialEval ((PRec n f) $> (PSucc n')) = ((CRec n f) $> (PSucc n'))

    -- partially evaluating each step
    -- partialEval ((CRec n f) $> (PSucc n')) = partialEval (f $> n') $> partialEval ((CRec n f) $> n')

    -- unrolling, partially evaluating each step, evaluating the unrolled program
    -- partialEval ((CRec n f) $> (PSucc n')) = partialEval (partialEval (f $> n') $> partialEval ((CRec n f) $> n'))
    partialEval (x $> y) = let arg = partialEval  y 
                               fn = partialEval  x in
                               fn $> arg
                               -- case (computeValue fn, computeValue arg) of
                               --   (Just (\\ f), Just arg) => partialEval (substOne f arg)
                               --   _ => fn $> arg
    partialEval (x %> y) = let arg = partialEval  y 
                               fn = partialEval  x in
                               case (computeValue fn, computeValue arg) of
                                 (Just (\\ f), Just arg) => partialEval (substOne f arg)
                                 _ => fn $> arg
PAdd : {n : _} -> Prog n
PAdd = \\ \\ (CRec (^0) (\\ \\ PSucc (^0)) $> (^1))
  
Mult : {n : _} -> Prog n
Mult = \\ \\ CRec PZero (\\ (PAdd $> ^0)) $> ^1

Pow : {n : _} ->Prog n
Pow = \\ \\ CRec (PSucc PZero) (\\ (Mult $> (^0))) $> (^1)

Id : {n : Nat} -> Prog n
Id = \\ ^0

simpleProg : Prog 0 
simpleProg = CIf PTrue PTrue PFalse

   
ifProg : Prog 0
ifProg = \\ CIf (^0) Id PFalse

POne : {n : _} -> Prog n
POne = PSucc PZero

PTwo : {n : _} -> Prog n
PTwo = PSucc POne

-- testPAdd : Partial.partialEval (PAdd %> POne %> POne) === PTwo
-- testPAdd = Refl

-- testPPow : Partial.partialEval (Pow PTwo PTwo) === 
--            Mult $> (Mult $> PTwo $> (Mult $> PTwo)) $> POne

-- testFunctionIf : Partial.partialEval [] Partial.ifProg === PTrue
-- testFunctionIf = Refl

testFunctionIf' : Partial.partialEval (Partial.ifProg $> PTrue) === Id
testFunctionIf' = Refl

testFuncIf : Partial.partialEval (Partial.ifProg $> (Partial.ifProg $> PFalse)) === PFalse
testFuncIf = Refl


testPartialIf : Partial.partialEval Partial.simpleProg === PTrue
testPartialIf = Refl
{-
-}

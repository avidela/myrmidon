module Conversion

import Data.List

data Lam = Var String | App Lam Lam | Abs String Lam

data Lang = Lambda | SKI | Both
data SK = S | K | T SK SK

data Calc = BVar String | BApp Calc Calc | BAbs String Calc | BS | BK | BT Calc Calc

I : SK
I = T (T S K) K

freeVariables : Calc -> List String
freeVariables (BVar x) = [x]
freeVariables (BApp x y) = freeVariables x ++ freeVariables y
freeVariables (BAbs x y) = filter (/=x) (freeVariables y)
freeVariables BS = []
freeVariables BK = []
freeVariables (BT x y) = freeVariables x ++ freeVariables y

free : String -> Calc -> Bool
free x p = x `elem` (freeVariables p) 

translate : Calc -> Calc
translate (BVar x) = BVar x
translate (BApp x y) = BApp (translate x) (translate y)
translate (BAbs x y) = if not (free x y) 
                          then BApp BK (translate y)
                          else case y of
                                    (BApp e1 e2) => BApp (BApp BS (translate (BAbs x e1))) 
                                                         (translate (BAbs x e2))
                                    (BAbs z w) => translate (BAbs x (translate (BAbs z w)))
                                    (BVar x') => BApp (BApp BS BK) BK -- λx. x
                                    _ => ?error
translate BS = BS
translate BK = BK
translate (BT x y) = ?error2

fromLam : Lam -> Calc
fromLam (Var x) = BVar x
fromLam (App x y) = BApp (fromLam x) (fromLam y)
fromLam (Abs x y) = BAbs x (fromLam y)

toSK : Calc -> SK
toSK BS = S
toSK BK = K
-- toSK (BT x y) = T (toSK x) (toSK y)
toSK (BApp x y) = T (toSK x) (toSK y)
toSK _ = ?failToSK

convert : Lam -> SK
convert = toSK . translate . fromLam


Program : Lam
Program = Abs "x" (Abs "y" (App (Var "y") (Var "x")))

test : Convertion.convert Program  === T (T S (T K (T S I))) (T(T S (T K K)) I)
test = Refl


